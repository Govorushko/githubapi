package com.example.github.dagger

import com.example.github.data.GithubServiceRepository
import com.example.github.data.GithubServiceRepositoryImpl
import com.example.github.data.source.TrendingDataSource
import com.example.github.data.source.local.TrendingLocalDataSource
import com.example.github.data.source.remote.TrendingRemoteDataSource
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class DataSourceModule{

    @LocalDataSource
    @Singleton
    @Binds
    abstract fun provideLocalDataSource(source: TrendingLocalDataSource): TrendingDataSource

    @RemoteDataSource
    @Singleton
    @Binds
    abstract fun provideRemoteDataSource(source: TrendingRemoteDataSource): TrendingDataSource

    @Singleton
    @Binds
    abstract fun provideRepository(repository: GithubServiceRepositoryImpl): GithubServiceRepository
}