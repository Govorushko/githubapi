package com.example.github.presentation.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.github.R
import com.example.github.data.model.TrendingRepository
import com.example.github.databinding.ItemRepoInfoBinding

class TrendingRepositoriesAdapter(private val reposListener: OnReposItemListener) :
    BindableRecyclerViewAdapter<TrendingRepository, TrendingRepositoriesAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemRepoInfoBinding>(
            inflater,
            R.layout.item_repo_info,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(resources[position])
    }

    interface OnReposItemListener {
        fun onRepoClick(repo: TrendingRepository)
    }

    inner class ViewHolder(private val binding: ItemRepoInfoBinding) :
        BindableRecyclerViewHolder<TrendingRepository>(binding) {

        override fun bind(item: TrendingRepository) {

            if (item.language == null) {
                binding.language.visibility = View.GONE
            } else {
                this.itemView
                binding.language.visibility = View.VISIBLE
            }

            binding.model = item
            binding.root.setOnClickListener {
                reposListener.onRepoClick(item)
            }
        }
    }
}
