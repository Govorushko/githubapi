package com.example.github.dagger

import javax.inject.Qualifier

@Qualifier
@kotlin.annotation.Retention
annotation class RemoteDataSource
