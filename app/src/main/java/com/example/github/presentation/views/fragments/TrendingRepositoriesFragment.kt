package com.example.github.presentation.views.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.github.App
import com.example.github.R
import com.example.github.data.model.TrendingRepository
import com.example.github.databinding.TrendingRepositoriesFragmentBinding
import com.example.github.presentation.adapters.TestListAdapter
import com.example.github.presentation.decorators.VerticalRecyclerViewDecorator
import com.example.github.presentation.viewmodels.TrendingRepositoriesViewModel
import com.example.github.presentation.views.activities.RepoInfoActivity
import kotlinx.android.synthetic.main.trending_repositories_fragment.*
//import org.koin.androidx.viewmodel.ext.android.viewModel
import javax.inject.Inject

class TrendingRepositoriesFragment : Fragment(), TestListAdapter.OnReposItemListener {

    @Inject lateinit var viewModel: TrendingRepositoriesViewModel
    private lateinit var binding: TrendingRepositoriesFragmentBinding
    private lateinit var testReposAdapter: TestListAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity!!.application as App).component.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.trending_repositories_fragment, container,false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initResources()
        iniRecyclerView()

        viewModel.trendRepos.observe(viewLifecycleOwner, Observer { testReposAdapter.setResources(it) })
    }

    private fun initResources() {
        binding.apply {
            model = viewModel
            lifecycleOwner = viewLifecycleOwner
        }
    }

    private fun iniRecyclerView() {
        testReposAdapter = TestListAdapter(this) { item -> this.onRepoClick(item) }

        trendingRepos_rv_fragment.addItemDecoration(VerticalRecyclerViewDecorator(8))
        trendingRepos_rv_fragment.adapter = testReposAdapter
    }

    override fun onRepoClick(repo: TrendingRepository) {
        val intent = Intent(context, RepoInfoActivity::class.java)
        intent.putExtra(EXTRA_SELECTED_REPO, repo)
        startActivity(intent)
    }

    companion object {
        fun newInstance() = TrendingRepositoriesFragment()
        const val EXTRA_SELECTED_REPO = "SELECTED_REPO"
    }
}