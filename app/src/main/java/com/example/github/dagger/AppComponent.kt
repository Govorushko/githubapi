package com.example.github.dagger

import androidx.work.ListenableWorker
import com.example.github.presentation.views.fragments.TrendingRepositoriesFragment
import com.example.github.workers.AppWorkerFactory
import com.example.github.workers.ChildWorkerFactory
import com.example.github.workers.RefreshRepositoriesWorker
import com.squareup.inject.assisted.dagger2.AssistedModule
import dagger.Binds
import dagger.Component
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Singleton
import kotlin.reflect.KClass

@Singleton
@Component(
    modules = [
        AppModule::class,
        DataSourceModule::class,
        DbModule::class,
        NetworkModule::class,
        WorkerBindingModule::class,
        AppAssistedInjectModule::class
    ]
)
interface AppComponent {
    fun inject(fragment: TrendingRepositoriesFragment)
    fun workerFactory(): AppWorkerFactory
}

@Module(includes = [AssistedInject_AppAssistedInjectModule::class])
@AssistedModule
abstract class AppAssistedInjectModule {}

@MapKey
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class WorkerKey(val value: KClass<out ListenableWorker>)

@Module
interface WorkerBindingModule {
    @Binds
    @IntoMap
    @WorkerKey(RefreshRepositoriesWorker::class)
    fun bindRefreshWorker(factory: RefreshRepositoriesWorker.Factory): ChildWorkerFactory
}