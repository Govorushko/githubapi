package com.example.github.data

import com.example.github.dagger.LocalDataSource
import com.example.github.dagger.RemoteDataSource
import com.example.github.data.model.TrendingRepository
import com.example.github.data.source.TrendingDataSource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

class GithubServiceRepositoryImpl
@Inject constructor(
    @RemoteDataSource private val remote: TrendingDataSource,
    @LocalDataSource private val local: TrendingDataSource
) : GithubServiceRepository {

    override fun getRepos(): Flow<List<TrendingRepository>> =
        local.getRepos().onEach {
            if (it.isNullOrEmpty()) refreshFromRemote()
        }

    override suspend fun refreshFromRemote() {
        val remoteList = remote.getRepos().first()
        val localList = local.getRepos().first()

        if (localList == remoteList) return

        local.setTrendingRepositories(remoteList)
    }

    override suspend fun dropDb() {
        local.dropDb()
    }
}