package com.example.github.presentation.adapters

import androidx.recyclerview.widget.RecyclerView

abstract class BindableRecyclerViewAdapter<S, VH : BindableRecyclerViewHolder<*>> :
    RecyclerView.Adapter<VH>() {

    protected var resources = ArrayList<S>()

    open fun setResources(new: List<S>) {
        resources.apply {
            clear()
            addAll(new)
        }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = resources.size
}
