package com.example.github.data.source.remote

import com.example.github.data.model.TrendingRepository
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET

interface CrackedApiService{

    @GET("repositories")
    suspend fun getTrendingRepos(): List<TrendingRepository>
}