package com.example.github.data.source

import com.example.github.data.model.TrendingRepository
import kotlinx.coroutines.flow.Flow

interface TrendingDataSource {
    fun getRepos(): Flow<List<TrendingRepository>>
    suspend fun setTrendingRepositories(newTrendRepos: List<TrendingRepository>): Unit = throw UnsupportedOperationException()
    suspend fun dropDb(): Unit = throw UnsupportedOperationException()
}