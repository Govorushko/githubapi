package com.example.github.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.example.github.data.model.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.util.*

@Dao
interface TrendRepoDao {
    fun getRepos(): Flow<List<TrendingRepository>> = getLocalRepos().map { list-> list.map { it -> it.toRemote()} }

    suspend fun setTrendRepos(newTrendRepos: List<TrendingRepository>) {
        dropRepos()
        for (repo in newTrendRepos) {
            val uuid = UUID.randomUUID()

            setLocalTrendRepos(repo.toLocal(uuid))

            for (contributor in repo.builtBy) {
                setLocalContributors(contributor.toLocal(uuid))
            }
        }
    }

    @Transaction
    @Query("SELECT * FROM localtrendingrepository")
    fun getLocalRepos(): Flow<List<LocalRepo>>

    @Query("DELETE FROM localtrendingrepository")
    suspend fun dropRepos()

    @Transaction
    @Insert
    suspend fun setLocalTrendRepos(localRepo: LocalTrendingRepository)

    @Transaction
    @Insert
    suspend fun setLocalContributors(localContributor: LocalContributor)
}