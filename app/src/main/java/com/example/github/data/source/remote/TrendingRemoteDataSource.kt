package com.example.github.data.source.remote

import com.example.github.data.model.TrendingRepository
import com.example.github.data.source.TrendingDataSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.launch
import javax.inject.Inject

class TrendingRemoteDataSource
@Inject constructor(
    private val service: CrackedApiService
) : TrendingDataSource {

    override fun getRepos(): Flow<List<TrendingRepository>> =
        flow { emit(service.getTrendingRepos()) }
}