package com.example.github.data.model

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class TrendingRepository(
    val name: String,
    val author: String,
    val avatar: String,
    val url: String,
    val description: String,
    val language: String?,
    val languageColor: String?,
    val stars: String,
    val forks: String,
    val currentPeriodStars: String,
    val builtBy: List<Contributor>
) : Parcelable

@Parcelize
data class Contributor(
    val username: String,
    val href: String,
    val avatar: String
) : Parcelable

@Entity
data class LocalTrendingRepository(
    @PrimaryKey
    val id: String,
    val name: String,
    val author: String,
    val avatar: String,
    val url: String,
    val description: String,
    val language: String?,
    val languageColor: String?,
    val stars: String,
    val forks: String,
    val currentPeriodStars: String
)

@Entity
data class LocalContributor(
    @PrimaryKey
    val id: String,
    val username: String,
    val href: String,
    val avatar: String,
    val repo_id: String
)

data class LocalRepo(
    @Embedded
    val repo: LocalTrendingRepository,

    @Relation(parentColumn = "id", entityColumn = "repo_id")
    val contributors: List<LocalContributor>
)

fun TrendingRepository.toLocal(id: UUID): LocalTrendingRepository {
    return LocalTrendingRepository(
        id.toString(),
        this.name,
        this.author,
        this.avatar,
        this.url,
        this.description,
        this.language,
        this.languageColor,
        this.stars,
        this.forks,
        this.currentPeriodStars
    )
}
fun LocalTrendingRepository.toRemote(list: List<Contributor>): TrendingRepository {
    return TrendingRepository(
        this.name,
        this.author,
        this.avatar,
        this.url,
        this.description,
        this.language,
        this.languageColor,
        this.stars,
        this.forks,
        this.currentPeriodStars,
        list
    )
}

fun Contributor.toLocal(parentId: UUID): LocalContributor {
    return LocalContributor(
        UUID.randomUUID().toString(),
        this.username,
        this.href,
        this.avatar,
        parentId.toString()
    )
}
fun LocalContributor.toRemote(): Contributor {
    return Contributor(
        this.username,
        this.href,
        this.avatar
    )
}

fun LocalRepo.toRemote(): TrendingRepository {

    val contributors = contributors.map { it.toRemote() }

    return repo.toRemote(contributors)
}
