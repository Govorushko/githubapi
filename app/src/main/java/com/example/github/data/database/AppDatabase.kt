package com.example.github.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.github.data.database.dao.TrendRepoDao
import com.example.github.data.model.*

@Database(entities = [LocalTrendingRepository::class, LocalContributor::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun trendRepoDao(): TrendRepoDao
}