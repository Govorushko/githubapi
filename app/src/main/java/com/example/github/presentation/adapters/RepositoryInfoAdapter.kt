package com.example.github.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.example.github.R
import com.example.github.data.model.Contributor
import com.example.github.databinding.ItemContributorInfoBinding

class RepositoryInfoAdapter :
    BindableRecyclerViewAdapter<Contributor, RepositoryInfoAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemContributorInfoBinding>(
            inflater,
            R.layout.item_contributor_info,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contributor = resources[position]
        holder.bind(contributor)
    }

    class ViewHolder(private val binding: ItemContributorInfoBinding) :
        BindableRecyclerViewHolder<Contributor>(binding) {
        override fun bind(contributor: Contributor) {
            binding.model = contributor
            Glide.with(binding.root).load(contributor.avatar).circleCrop().into(binding.contributorAvatar)
        }
    }
}