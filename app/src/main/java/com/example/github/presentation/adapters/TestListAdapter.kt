package com.example.github.presentation.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.github.R
import com.example.github.data.model.TrendingRepository
import com.example.github.databinding.ItemRepoInfoBinding
import com.example.github.presentation.adapters.TestListAdapter.ViewHolder

class TestListAdapter(private val reposListener: OnReposItemListener, private val callback: (TrendingRepository) -> Unit) :
    ListAdapter<TrendingRepository, ViewHolder>(DIFF_CALLBACK) {

    fun setResources(new: List<TrendingRepository>){
        submitList(new)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemRepoInfoBinding>(
            inflater,
            R.layout.item_repo_info,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    interface OnReposItemListener {
        fun onRepoClick(repo: TrendingRepository)
    }

    inner class ViewHolder(private val binding: ItemRepoInfoBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: TrendingRepository) {

            if (item.language == null) {
                binding.language.visibility = View.GONE
            } else {
                this.itemView
                binding.language.visibility = View.VISIBLE
            }

            binding.model = item
            binding.root.setOnClickListener {
                reposListener.onRepoClick(item)
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<TrendingRepository>() {
            override fun areItemsTheSame(oldItem: TrendingRepository, newItem: TrendingRepository): Boolean = oldItem.url == newItem.url

            override fun areContentsTheSame(oldItem: TrendingRepository, newItem: TrendingRepository): Boolean = oldItem == newItem
        }
    }
}
