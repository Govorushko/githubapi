package com.example.github.dagger

import com.example.github.BuildConfig
import com.example.github.data.source.remote.CrackedApiService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideRetrofit(): CrackedApiService {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.CRACKED_BASE_URL)
//            .baseUrl("https://private-anon-2792820608-githubtrendingapi.apiary-mock.com") /*mock-server*/
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(CrackedApiService::class.java)
    }


}