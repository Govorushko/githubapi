package com.example.github.presentation.decorators

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class VerticalRecyclerViewDecorator(private val spacing: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, itemPosition: Int, parent: RecyclerView) {
        super.getItemOffsets(outRect, itemPosition, parent)

        outRect.left = spacing
        outRect.top = spacing / 2
        outRect.right = spacing
        outRect.bottom = spacing / 2
    }
}