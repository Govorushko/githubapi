package com.example.github.data.source.local

import com.example.github.data.database.AppDatabase
import com.example.github.data.model.TrendingRepository
import com.example.github.data.source.TrendingDataSource
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class TrendingLocalDataSource
@Inject constructor(
    private val db: AppDatabase
) : TrendingDataSource {

    override fun getRepos(): Flow<List<TrendingRepository>> =
        db.trendRepoDao().getRepos()

    override suspend fun setTrendingRepositories(newTrendRepos: List<TrendingRepository>) {
        db.trendRepoDao().setTrendRepos(newTrendRepos)
    }

    override suspend fun dropDb() = db.trendRepoDao().dropRepos()
}