package com.example.github

import android.app.Application
import androidx.work.Configuration
import androidx.work.WorkManager
import com.example.github.dagger.*
import com.example.github.workers.AppWorkerFactory
import javax.inject.Inject

class App : Application() {
    lateinit var component: AppComponent

    override fun onCreate() {
        super.onCreate()
        instance = this

        initDagger()
        initWorkerManager()
    }

    private fun initDagger() {
        component =
            DaggerAppComponent.builder()
                .dbModule(DbModule())
                .networkModule(NetworkModule())
                .build()
    }

    private fun initWorkerManager() {
        val myWorkerFactory: AppWorkerFactory = DaggerAppComponent.create().workerFactory()

        WorkManager.initialize(
            this,
            Configuration.Builder()
                .setWorkerFactory(myWorkerFactory)
                .build()
        )
    }

    companion object {
        lateinit var instance: Application
    }
}