package com.example.github.dagger

import androidx.room.Room
import com.example.github.App
import com.example.github.data.database.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule {

    @Singleton
    @Provides
    fun provideDatabase(): AppDatabase = Room.databaseBuilder(
        App.instance.applicationContext,
        AppDatabase::class.java,
        DATABASE_NAME
    ).build()

    companion object {

        const val DATABASE_NAME = "github_database"
    }
}