package com.example.github.workers

import android.content.Context
import androidx.work.*
import com.example.github.data.GithubServiceRepository
import com.squareup.inject.assisted.Assisted
import com.squareup.inject.assisted.AssistedInject
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@ExperimentalCoroutinesApi
class RefreshRepositoriesWorker @AssistedInject constructor(
    @Assisted private val ctx: Context,
    @Assisted private val params: WorkerParameters,
    var repo: GithubServiceRepository
) : CoroutineWorker(ctx, params) {

    override suspend fun doWork(): Result = coroutineScope {
        try {
            delay(5000L)
            repo.refreshFromRemote()
            Result.success()
        } catch (t: Throwable) {
            Result.failure()
        }
    }

    companion object {
        const val WORKER_NAME = "TREND_REPOS_WORKER"

        fun run() {
            val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

            val work = PeriodicWorkRequest.Builder(
                    RefreshRepositoriesWorker::class.java,
                    15,
                    TimeUnit.MINUTES
                )
                .setConstraints(constraints)
                .build()

            WorkManager.getInstance()
                .enqueueUniquePeriodicWork(WORKER_NAME, ExistingPeriodicWorkPolicy.REPLACE, work)
        }
    }

    @AssistedInject.Factory
    interface Factory : ChildWorkerFactory
}

