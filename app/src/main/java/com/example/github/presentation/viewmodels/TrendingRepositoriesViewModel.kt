package com.example.github.presentation.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.work.*
import com.example.github.data.GithubServiceRepository
import com.example.github.data.model.Contributor
import com.example.github.data.model.TrendingRepository
import com.example.github.workers.RefreshRepositoriesWorker
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class TrendingRepositoriesViewModel @Inject constructor(repo: GithubServiceRepository) : ViewModel() {
    val trendRepos: LiveData<List<TrendingRepository>> =
        repo.getRepos().flowOn(Dispatchers.IO).asLiveData()
}