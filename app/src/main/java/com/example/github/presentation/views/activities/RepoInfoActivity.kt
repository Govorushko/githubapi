package com.example.github.presentation.views.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.github.R
import com.example.github.data.model.TrendingRepository
import com.example.github.databinding.ActivityRepoInfoBinding
import com.example.github.presentation.adapters.RepositoryInfoAdapter
import com.example.github.presentation.decorators.VerticalRecyclerViewDecorator
import com.example.github.presentation.viewmodels.RepoInfoActivityViewModel
import com.example.github.presentation.views.fragments.TrendingRepositoriesFragment
import kotlinx.android.synthetic.main.activity_repo_info.*

class RepoInfoActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRepoInfoBinding
    private lateinit var repoViewModel: RepoInfoActivityViewModel
    private lateinit var contributorsListAdapter: RepositoryInfoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initResources()
        initViewModel()
        initUi()
    }

    private fun initResources() {
        repoViewModel = ViewModelProvider(this).get(RepoInfoActivityViewModel::class.java)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_repo_info)
        binding.apply {
            model = repoViewModel
            lifecycleOwner = this@RepoInfoActivity
        }

    }

    private fun initViewModel() {
        val repo =
            intent.extras?.get(TrendingRepositoriesFragment.EXTRA_SELECTED_REPO) as TrendingRepository

        if (repo.language == null){
            repoLanguage.visibility = View.GONE
        }

        repoViewModel.repo.value = repo
    }

    private fun initUi() {
        contributorsListAdapter = RepositoryInfoAdapter()
        contributorsList.addItemDecoration(VerticalRecyclerViewDecorator(8))
        contributorsList.adapter = contributorsListAdapter
        contributorsListAdapter.setResources(repoViewModel.repo.value!!.builtBy)
    }

}
