package com.example.github.presentation.views.activities

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.github.App
import com.example.github.R
import com.example.github.presentation.views.fragments.TrendingRepositoriesFragment
import com.example.github.workers.RefreshRepositoriesWorker
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(bottom_app_bar)
        RefreshRepositoriesWorker.run()

        showRepos()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.bottomappbar_menu, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.appbar_repos -> showRepos()
        }

        return true
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 0) {
            finish()
        } else {
            supportFragmentManager.popBackStack()
            changeBottomBarVisibility()
        }
    }

    private fun showRepos() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainer, TrendingRepositoriesFragment.newInstance())
            addToBackStack(null)
            commit()
        }

        changeBottomBarVisibility()
    }

    private fun changeBottomBarVisibility() {
        if (bottom_app_bar.visibility == View.VISIBLE) {
            fab.visibility = View.INVISIBLE
            bottom_app_bar.visibility = View.INVISIBLE
            return
        }

        bottom_app_bar.visibility = View.VISIBLE
        fab.visibility = View.VISIBLE
    }
}