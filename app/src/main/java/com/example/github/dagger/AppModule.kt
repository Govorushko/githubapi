package com.example.github.dagger

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(context: Context) {

    private val context: Context by lazy { context.applicationContext }

    @Singleton
    @Provides
    fun provideAppContext(): Context = context
}