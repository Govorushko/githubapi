package com.example.github.workers

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.ListenableWorker
import androidx.work.WorkerParameters

interface ChildWorkerFactory {
    fun create(ctx: Context, params: WorkerParameters): ListenableWorker
}