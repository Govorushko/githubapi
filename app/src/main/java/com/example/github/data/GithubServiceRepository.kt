package com.example.github.data

import com.example.github.data.model.TrendingRepository
import kotlinx.coroutines.flow.Flow

interface GithubServiceRepository {
    fun getRepos(): Flow<List<TrendingRepository>>
    suspend fun refreshFromRemote()
    suspend fun dropDb()
}