package com.example.github.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.github.data.model.TrendingRepository

class RepoInfoActivityViewModel : ViewModel() {
    var repo: MutableLiveData<TrendingRepository> = MutableLiveData()

}