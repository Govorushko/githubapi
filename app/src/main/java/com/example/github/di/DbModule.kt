package com.example.github.di

import androidx.room.Room
import com.example.github.App
import com.example.github.data.database.AppDatabase
//import org.koin.dsl.module
//
//val databaseModule = module {
//
//    fun provideDatabase(): AppDatabase {
//        return Room.databaseBuilder(
//            App.instance.applicationContext,
//            AppDatabase::class.java,
//            "github_database"
//        ).build()
//    }
//
//    single { provideDatabase() }
//}