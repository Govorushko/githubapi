package com.example.github.di

import com.example.github.data.GithubServiceRepository
import com.example.github.data.GithubServiceRepositoryImpl
import com.example.github.data.source.TrendingDataSource
//import com.example.github.data.source.TrendingDataSource
import com.example.github.data.source.local.TrendingLocalDataSource
import com.example.github.data.source.remote.TrendingRemoteDataSource
import kotlinx.coroutines.ExperimentalCoroutinesApi

//@ExperimentalCoroutinesApi
//val dataSourceModule = module {
//    single<TrendingDataSource>(named<TrendingLocalDataSource>()) { TrendingLocalDataSource(get()) }
//    single<TrendingDataSource>(named<TrendingRemoteDataSource>()) { TrendingRemoteDataSource(get()) }
//    single<GithubServiceRepository> { GithubServiceRepositoryImpl(get(named<TrendingRemoteDataSource>()), get(named<TrendingLocalDataSource>())) }
//}